export const environment = {
  production: true,
  API_BASE_PATH:"http://185.226.160.93:9090/api",

  firebase: {},


  debug: true,
  log: {
    auth: false,
    store: false,
  },

  smartadmin: {
    api: null,
    db: 'smartadmin-angular'
  }
};
