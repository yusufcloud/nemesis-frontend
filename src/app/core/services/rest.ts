import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { NotifierService } from 'angular-notifier';
import { Router } from "@angular/router";
import { map, count, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable()
export class RestProvider {
  e: any = [];
  a: any = [];
  b: any = [];
  private readonly notifier: NotifierService;

  constructor(public http: HttpClient, notifierService: NotifierService, public router: Router) {
    console.log('Rest connection');
    this.notifier = notifierService;
  }

  registerUser(user) {
    if(user.terms == false){
      this.notifier.show({ type: 'error', message: 'Lütfen Üyelik Sözleşmesini Okuyup Kabul Ediniz' });
    } else {
    this.http.post(environment.API_BASE_PATH + '/register/token', user)
      .subscribe(res => {
        console.log('Register: ' + res);
        this.notifier.show({ type: 'success', message: 'Giriş Yapabilmek İçin Mail Aktivasyonunuzu yapmanız gerekmektedir.' });
        this.notifier.show({ type: 'success', message: 'Başarı İle Kayıt Oldunuz Giriş Ekranına Yönlendiriliyorsunuz.' });
        
        setTimeout(() => {
          this.router.navigateByUrl('/auth/login');
        }, 1500)
      });


    }
  }

  login(email: string, password: string) {
    console.log('Login');
    console.log(environment.API_BASE_PATH,'/login/token', { email, password });
    return this.http.post<any>(environment.API_BASE_PATH + '/login/token', { email, password })
      .pipe(map(user => {
        console.log('User: ' + user);
        if (user && user.token) {
          localStorage.setItem("userToken", user.token);
          localStorage.setItem("email", user.email);
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem("firstName", user.firstName);
          localStorage.setItem("lastName", user.lastName);
          this.router.navigateByUrl('/');
        }
        else {
          this.notifier.show({ type: 'error', message: this.e.message });
        }
        return user;
      }),catchError(this.handleError<any[]>('getHeroes', []))).subscribe();
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      
      this.notifier.show({ type: 'error', message: 'E-mail veya şifre hatalı.' });
   
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  loginUser(user) {
    return new Promise(resolve => {
      this.http.post(environment.API_BASE_PATH + "/login", user).subscribe(data => {
        resolve(data);
        this.e = data;
        console.log(this.e);
        if (this.e.status == false) {
          this.notifier.show({ type: 'error', message: this.e.message });
        }
        else {
          this.notifier.show({ type: 'success', message: this.e.message });
          localStorage.setItem("userToken", this.e.token);
          localStorage.setItem("firstName", this.e.user.firstName);
          localStorage.setItem("lastName", this.e.user.lastName);
          localStorage.setItem("email", this.e.user.email);
          if (this.e.status == true) {
            setTimeout(() => {
              this.router.navigateByUrl('/');
            }, 1500)
          }
        }

      }, err => {
        console.log(err);
      });
    });
  }

  forgotPassword(email) {
    return new Promise(resolve => {
      this.http.put(environment.API_BASE_PATH + "/forgot", email).subscribe(data => {
        resolve(data);
        console.log(data);
      }, err => {
        console.log('err ', err);
      });
    });
  };

  test(email) {
    this.http.put(environment.API_BASE_PATH + "/forgot", email)
      .subscribe(res => {
        console.log(res);
      })
  }


  public getTweet(name: string, count: number) {
    return new Promise(resolve => {
      this.http.get(environment.API_BASE_PATH + '/tweets/user/' + name + "/" + count).subscribe(data => {
        console.log(environment.API_BASE_PATH + '/tweets/user/' + name + "/" + count);
        console.log('Data: ' + data);
        resolve(data);
      }, err => {
        console.log("getTweet Error", err)
         if (err.status === 404) {
           this.notifier.notify('error', 'Lütfen Tweet Kutusuna Bir Değer Giriniz.!');
         }
        if (err.status === 500) {
          this.notifier.notify('error', 'Böyle Bir Kullanıcı Yok veya Hesabı Gizlidir Lütfen Farklı Hesap deneyiniz.');
        }

        // alert("Girilen Hesap Gizli Bir Hesap Lütfen Başka Bir Hesap Deneyiniz.!");
        // console.log("tweet-error", err);
      });
    });
  }


  public getHastag(name: string, count: number, type: number) {
    return new Promise(resolve => {
      this.http.get<any>(environment.API_BASE_PATH + "/tweets/" + name + "/" + count + "/" + type).subscribe(data => {
        console.log(environment.API_BASE_PATH + "/tweets/" + name + "/" + count + "/" + type);
        console.log('Data: ' + data);
        if(data.tweets.length == 0){
          this.notifier.notify('error', 'Bu Hashtag İle İlgili Hiçbir Tweet Bulunamadı.');
        }
        resolve(data);
      }, err => {
        console.log("getTweet Error", err)
        if (err.url === null) {
          this.notifier.notify('error', 'Böyle Bir Kullanıcı Yok veya Hesabı Gizlidir Lütfen Farklı Hesap deneyiniz.');
        }
      });
    });
  }

  public getTrend(token: any) {
    return new Promise(resolve => {
      this.http.get(environment.API_BASE_PATH + "/trend" + "?token=" + token)
        .subscribe(data => {
          console.log("PathgetTrend: " + environment.API_BASE_PATH + "/trend" + "?token=" + token);
          this.b = data
          resolve(this.b);
          // if (this.b.message == 'Faild To Authenticate token') {
          //   this.notifier.show({ type: 'error', message: 'Önce Oturum Açmanız Gerekmektedir. Yönlendiriliyorsunuz.!' });
          //   setTimeout(() => {
          //     this.router.navigateByUrl('auth/login');
          //   }, 1000)
          // }
        }, err => {
          console.log("getTrend Error", err);
        });
    });
  }

  createProject(projectName: string, projectTitle: string, extraInfo: string, comments: string,
    startDate: string, deadline: string) {
    console.log(environment.API_BASE_PATH + '/project', {
      projectName, projectTitle, extraInfo, comments,
      startDate, deadline
    });
    this.http.post(environment.API_BASE_PATH + '/project', {
      projectName, projectTitle, extraInfo, comments,
      startDate, deadline
    })
      .subscribe(res => {
        console.log(res);
      })
  }


}
