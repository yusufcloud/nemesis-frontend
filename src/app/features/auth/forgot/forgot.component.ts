import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { RestProvider } from '../../../core/services/rest';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styles: []
})
export class ForgotComponent implements OnInit {

  constructor(private router: Router, public rest: RestProvider) {
    // rest.forgotPassword('emiroztrrk@gmail.com')
    //   .then((data: any) => {
    //     console.log(data);
    //   });
  }

  ngOnInit() {

  }

  forgotPassword(password) {
    console.log(password.value);
    this.rest.test(password.value);
  }

  submit(event) {
    event.preventDefault();
    this.router.navigate(['/dashboard/+analytics'])
  }
}
