import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotRoutingModule } from './forgot-routing.module';
import { ForgotComponent } from './forgot.component';
import { RestProvider } from '../../../core/services/rest';
import { FormsModule } from '@angular/forms';
import {I18nModule} from "@app/shared/i18n/i18n.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ForgotRoutingModule,
    I18nModule
  ],
  declarations: [ForgotComponent],
  providers: [
    RestProvider
  ]
})
export class ForgotModule { }
