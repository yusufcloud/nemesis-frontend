import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from "@angular/router";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RestProvider } from '../../../core/services/rest';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: []
})
export class RegisterComponent implements OnInit {


  bsModalRef: BsModalRef;
  public termsAgreed = false

  constructor(
    private router: Router,
    private modalService: BsModalService,
    private restProvider: RestProvider,
    private notifierService: NotifierService
  ) { }

  ngOnInit() { }

  register(event) {
    event.preventDefault();
    this.router.navigate(['/dashboard'])
  }

  openModal(event, template: TemplateRef<any>) {
    event.preventDefault();
    this.bsModalRef = this.modalService.show(template);
  }

  onTermsAgree() {
    this.termsAgreed = true
    this.bsModalRef.hide()
  }

  onTermsClose() {
    this.bsModalRef.hide()
  }

  registerUser(user) {
    console.log(user.value);
    this.restProvider.registerUser(user.value);
    // this.notifierService.notify('success', 'Başarı İle Kayıt Oldunuz Giriş Ekranına Yönlendiriliyorsunuz.');
    // setTimeout(() => {
    //   this.router.navigateByUrl('/auth/login');
    // }, 1500)
  }


}
