import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { RestProvider } from '../../../core/services/rest';

export interface Login {
  email: string;
  password: string;
  kaydet: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  model: Login = { email: '', password: '', kaydet: '' };

  constructor(private router: Router,
    private restProvider: RestProvider) { }

  ngOnInit() {
    const kaydet = localStorage.getItem("kaydet");
    this.model.kaydet = kaydet;

    if (kaydet == 'true') {
      const email = localStorage.getItem("email");
      const password = localStorage.getItem("password");
      this.model.email = email;
      this.model.password = password;
    }
  }

  login(event) {
    event.preventDefault();
    this.router.navigate(['/dashboard'])
  }

  loginUser(user) {
    // console.log(user.value);
    this.restProvider.login(user.value.email, user.value.password);
    localStorage.setItem("password", user.value.password)
    localStorage.setItem("kaydet", user.value.kaydet)
  }
}
