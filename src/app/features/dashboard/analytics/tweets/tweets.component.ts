import { Component, OnInit } from '@angular/core';
import { DatePipe } from "@angular/common";
import { Store } from '@ngrx/store';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import * as fromCalendar from "@app/core/store/calendar";
import { RestProvider } from '../../../../core/services/rest';
import { JsonApiService } from '@app/core/services';
import { NotifierService } from 'angular-notifier';
import { Router } from "@angular/router";


@Component({
  selector: 'sa-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css']
})
export class TweetsComponent implements OnInit {
  tweets: any = [];
  hastags: any = [];
  tweetType: any;
  trend: any;
  trendsName: any;
  hashtags: any = [];

  count: number = 1;
  hcount: number = 1;
  analysis = false;

  tweet = '';
  hastag = '';
  public chartjsData: any;


  public calendar$
  constructor(
    private store: Store<any>,
    private http: HttpClient,
    public restProvider: RestProvider,
    private jsonApiService: JsonApiService,
    public datePipe: DatePipe,
    private notifierService: NotifierService,
    public router: Router
  ) {
    this.calendar$ = this.store.select(fromCalendar.getCalendarState);
  }

  ngOnInit() {
    this.jsonApiService.fetch('/graphs/chartjs.json').subscribe((data) => {
      this.chartjsData = data;
    });
    // setTimeout(() => {
    //   this.getTrend();
    // }, 1000);
  }

  getTweet() {
    this.restProvider.getTweet(this.tweet, this.count).then((data: any) => {
      this.tweets = data;
      console.log("tweet-data", data);
      if (data.message == 'Faild To Authenticate token') {
        this.notifierService.show({ type: 'error', message: 'Önce Oturum Açmanız Gerekmektedir. Yönlendiriliyorsunuz.!' });
        this.router.navigateByUrl('auth/login');
      }
      this.doAnalysis(this.tweets);
      this.analysis = true;
      if (this.tweets.length < 1) {
        this.notifierService.show({ type: 'error', message: 'Kullanıcının Hiç Tweeti bulunmamaktadır.!' })
      }
      else {
        this.notifierService.show({ type: 'success', message: 'Tweetler Başarı İle Getirildi.!' });
      }
    });
  }

  getHastag() {
    this.restProvider.getHastag(this.hastag, this.hcount, this.tweetType)
      .then((data: any) => {
        this.hastags = data;
        console.log("hastag-data", data);
        if (data.statuses == false) {
          this.notifierService.show({ type: 'error', message: 'Önce Oturum Açmanız Gerekmektedir. Yönlendiriliyorsunuz.!' });
          this.router.navigateByUrl('auth/login');
        }
        // this.analysis = true;
        if (this.hastags.length < 1) {
          this.notifierService.notify('error', 'Hashtag Boş Başka Bir Hashtag Arayınız.!');
        }
      });
  }

  dates: any[] = [];
  dateDiffTotal = 0;
  lineChartJsData = {
    "labels": [],
    "datasets": [{
      "label": "Tweets",
      "data": [],
      "backgroundColor": "rgba(220,220,220,0.2)",
      "borderColor": "rgba(220,220,220,1)",
      "pointBackgroundColor": "rgba(220,220,220,1)"
    }]
  };

  doAnalysis(twts) {
    this.dates = [];
    let myDate: any = new Date();
    let cnt = 1;

    let d = this.datePipe.transform(twts[0].createdAt, 'shortDate');
    console.log('d: ' + d);
    for (let i = 0; i < twts.length; i++) {
      const dummyDate = {
        date: '',
        last: 0
      };

      if (i < twts.length - 1) {
        dummyDate.date = twts[i].createdAt;
        const diff = (myDate - this.convertDate(twts[i].createdAt)) / 86400000;
        this.dateDiffTotal += diff;
        dummyDate.last = Math.round(diff);

      } else {
        dummyDate.date = twts[i].createdAt;
        dummyDate.last = Math.round((myDate - this.convertDate(twts[i].createdAt)) / 86400000);
      }

      if (i > 0) {
        if (d == this.datePipe.transform(dummyDate.date, 'shortDate')) {
          cnt++;
          d = this.datePipe.transform(dummyDate.date, 'shortDate');
        }
        else {
          this.lineChartJsData.labels.push(this.datePipe.transform(dummyDate.date, 'shortDate'));
          this.lineChartJsData.datasets[0].data.push(cnt);
          cnt = 1;
          d = this.datePipe.transform(dummyDate.date, 'shortDate');
        }
      }
      else if (i == twts.length - 1) {

      }
      else {
        this.lineChartJsData.labels.push(this.datePipe.transform(d, 'shortDate'));
      }

      this.dates.push(dummyDate);
    }
    console.log('labels: ' + this.lineChartJsData.labels);
    console.log('data: ' + this.lineChartJsData.datasets[0].data);
  }

  convertDate(date) {
    return new Date(date).getTime();
  }

}
