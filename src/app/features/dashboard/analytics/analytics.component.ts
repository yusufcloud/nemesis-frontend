import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { HttpClient } from "@angular/common/http";
import * as fromCalendar from "@app/core/store/calendar";

@Component({
  selector: 'sa-analytics',
  templateUrl: './analytics.component.html',
})
export class AnalyticsComponent implements OnInit {

  public calendar$
  constructor(
    private store: Store<any>,
    private http: HttpClient,
  ) {
    this.calendar$ = this.store.select(fromCalendar.getCalendarState);
  }

  ngOnInit() {

  }

}
