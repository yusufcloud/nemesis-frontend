import {ModuleWithProviders} from "@angular/core"
import {Routes, RouterModule} from '@angular/router';


export const routes: Routes = [
  {
    path: 'project-create',
    loadChildren: './project-create/project-create.module#ProjectCreateModule'
  },
  {
    path: 'projects-list',
    loadChildren: './projects-list/projects-list.module#ProjectsListModule'
  }
];

export const routing = RouterModule.forChild(routes);
