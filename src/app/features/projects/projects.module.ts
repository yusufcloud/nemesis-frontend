import { NgModule } from "@angular/core";

import { routing } from "./projects.routing";

@NgModule({
  declarations: [],
  imports: [routing],
  providers: []
})
export class ProjectsModule {}
