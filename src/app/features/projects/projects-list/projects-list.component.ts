import {Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { RestProvider } from '@app/core/services/rest';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'app-projects',
  templateUrl: './projects-list.component.html',
})
export class ProjectsListComponent implements OnInit {
  public data: Object;
  public temp_var: Object=false;
  @ViewChild('dataTable') table;
  dataTable: any;
  constructor(private http: HttpClient){
  }

  ngOnInit(): void {
    this.http.get(environment.API_BASE_PATH + '/project/findAll').subscribe((res: Response) => {
        this.data=res;
        this.temp_var=true;
      });
  }
}
