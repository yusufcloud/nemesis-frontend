import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectCreateComponent } from './project-create.component';
import { ProjectCreateRoutingModule } from './project-create-routing.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { RestProvider } from '@app/core/services/rest';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ProjectCreateRoutingModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    FormsModule
  ],
  declarations: [ProjectCreateComponent],
  providers: [RestProvider]
})
export class ProjectCreateModule { }
