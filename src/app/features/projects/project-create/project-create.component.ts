import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestProvider } from '@app/core/services/rest';
import { NotifierService } from 'angular-notifier';

export interface Project {
  projectName: string;
  projectTitle: string;
  extraInfo: string;
  comments: string;
  startDate: Date;
  deadline: Date;
}

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html'
})
export class ProjectCreateComponent implements OnInit {

  model: Project = {
    projectName: '', projectTitle: '', extraInfo: '', comments: '',
    startDate: null, deadline: null
  };

  constructor(private router: Router,
    private restProvider: RestProvider,
    private notifierService: NotifierService) { }

  ngOnInit() {
    const projectName = localStorage.getItem("projectName");
    const projectTitle = localStorage.getItem("projectTitle");
    const extraInfo = localStorage.getItem("extraInfo");
    const comments = localStorage.getItem("comments");
    const startDate = localStorage.getItem("startDate");
    const deadline = localStorage.getItem("deadline");
    this.model.projectName = projectName;
    this.model.projectTitle = projectTitle;
    this.model.extraInfo = extraInfo;
    this.model.comments = comments;
    this.model.startDate = new Date(startDate);
    this.model.deadline = new Date(deadline);
  }

  createProject(project) {
    console.log('Deneme: ' + project.value);
    this.restProvider.createProject(project.value.projectName, project.value.projectTitle
      , project.value.extraInfo, project.value.comments, project.value.startDate
      , project.value.deadline);
    localStorage.setItem("projectName", project.value.projectName);
    localStorage.setItem("projectTitle", project.value.projectTitle);
    localStorage.setItem("extraInfo", project.value.extraInfo);
    localStorage.setItem("comments", project.value.comments);
    localStorage.setItem("startDate", project.value.startDate);
    localStorage.setItem("deadline", project.value.deadline);

    this.notifierService.notify('success', 'Project Created.');
    this.router.navigate(['/projects/projects-list']);
  }

}
