import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sa-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
  today = new Date();
  lastActivity = Math.floor((Math.random() * 59) + 1);

  constructor() {}

  ngOnInit() {}

}
