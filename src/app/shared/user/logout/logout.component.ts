import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NotificationService } from "@app/core/services/notification.service";

import { UserService } from "@app/core/services/user.service";

@Component({
  selector: "sa-logout",
  template: `
<div id="logout" (click)="showPopup()" class="btn-header transparent pull-right">
        <span> <a title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
    </div>
  `,
  styles: []
})
export class LogoutComponent implements OnInit {
  public user;

  constructor(
    private userService: UserService,
    private router: Router,
    private notificationService: NotificationService
  ) {
  }

  showPopup() {
    const firstName = localStorage.getItem("firstName");
    const lastName = localStorage.getItem("lastName");
    this.notificationService.smartMessageBox(
      {
        title:
          "<i class='fa fa-sign-out txt-color-orangeDark'></i> Çıkış Yapmak İstiyor musun  <span class='txt-color-orangeDark' style='text-transform= capitalize'><strong>" + firstName + " " + lastName + "</strong></span> ?",
        content:
          "Çıkış Yaptığında Tüm Verilerin Silinecektir.",
        buttons: "[Hayır][Evet]"
      },
      ButtonPressed => {
        if (ButtonPressed == "Evet") {
          this.logout();
          localStorage.removeItem("userToken");
          localStorage.removeItem("currentUser");
        }
      }
    );
  }

  logout() {
    this.userService.logout();
    this.router.navigate(["/auth/login"]);
  }

  ngOnInit() { }
}
