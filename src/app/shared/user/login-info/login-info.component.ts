import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/core/services/user.service';
import { LayoutService } from '@app/core/services/layout.service';
import { BehaviorSubject, of } from 'rxjs';

@Component({

  selector: 'sa-login-info',
  templateUrl: './login-info.component.html',
})
export class LoginInfoComponent implements OnInit {
  private userLogged: any;

  constructor(
    public us: UserService,
    private layoutService: LayoutService) {
  }

  ngOnInit() {
    if (localStorage.getItem('email')) {
      this.userLogged = {username: localStorage.getItem('email')};
    }
  }

  toggleShortcut() {
    console.log('Deactivated!');
    // this.layoutService.onShortcutToggle()
  }

}
